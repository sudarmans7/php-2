<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih Looping PHP</h1>

    <?php
        echo "<h3> Soal No 1 Looping I Love PHP </h3>";
        echo "<h5>LOOPING PERTAMA</h5>";
        for($o=2; $o<=20; $o+=2){
            echo $o . " --- I Love PHP <br>";
        }
        echo "<h5>LOOPING KEDUA</h5>";
        for($z=20; $z>=2; $z-=2){
            echo $z . " --- I Love PHP <br>";
        }

        echo "<h3> Soal No 2 Looping Array Modulo </h3>";
        $numbers = [18, 45, 29, 61, 47, 34];
        echo "Array numbers : ";
        echo "<br>";
        print_r($numbers);
        echo "<br>";
        echo "Hasil sisa bagi dari Array numbers";
        echo "<br>";
        foreach($numbers as $value){
            $rest[] = $value %= 5;
        }
        print_r($rest);
        echo "<br>";

        echo "<h3> Soal No 3 Looping Asociative Array </h3>";
        $biodata = [
            [001, "Keyboard Logitek", 60000, "Keyboard yang mantap untuk kantoran", "logitek.jpeg"],
            [002, "Keyboard MSI", 300000, "Keyboard gaming MSI mekanik", "msi.jpeg"],
            [003, "Mouse Genius", 50000, "Mouse Genius biar lebih pinter", "genius.jpeg"],
            [004, "Mouse Jerry", 30000, "Mouse yang disukai kucing", "jerry.jpeg"],
        ];

        foreach($biodata as $key => $value){
            $item = array(
                'id' => $value[0],
                'name' => $value[1],
                'price' => $value[2],
                'description' => $value[3],
                'source' => $value[4],
            );
            print_r($item);
            echo "<br>";
        }

        echo "<h3> Soal No 4 Asterix </h3>";
        for($s=1; $s<=5; $s++){
            for($u=1; $u<=$s; $u++){
                echo "*";
            }
            echo "<br>";
        }

    ?>


</body>
</html>